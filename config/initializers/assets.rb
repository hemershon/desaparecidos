Rails.application.config.assets.version = '1.0'
Rails.application.config.assets.paths << Rails.root.join('node_modules')
Rails.application.config.assets.precompile += %w( *.js *.scss devise/sessions.js devise/sessions.scss devise/registrations.js devise/registrations.scss devise/passwords.js devise/passwords.scss)
