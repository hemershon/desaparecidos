Rails.application.routes.draw do
  devise_for :users
  resources :historics
  resources :disappeareds
  resources :cities
  resources :parents
  resources :streets, only: :show

  root to: "home#index"
end
