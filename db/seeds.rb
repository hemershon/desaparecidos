Rails.env = "seed"
# Carregando seeds da pasta db/seeds
Dir[File.join(Rails.root, 'db', 'seeds', '*.rb')].sort.each do |seed|
  load seed
end

puts "creating district"
District.create(name: "alagoinhas", city_id: 3737)

puts "creating street"
Street.create(name: "rua ficticia", district_id: 1)

puts "creating user root"
User.create(email: "admin@desaparecidos", name: "Admin", telephone:"6666666666", address: "Porto Velho", password: "123456")

puts "creating disappeared"
Disappeared.create(user_id: 1, name: "matheus", gender: false, birthdate: 28042001, disappearance_date: 28052008, street_id: 1,mother: "dona", dad: "seu")
Disappeared.create(user_id: 1, name: "Andre", gender: false, birthdate: 28042001, disappearance_date: 28052008, street_id: 1,mother: "dona", dad: "seu")
