class CreateDisappeareds < ActiveRecord::Migration[5.2]
  def change
    create_table :disappeareds do |t|
      t.references :user, foreign_key: true
      t.string :name
      t.boolean :gender, default: false
      t.integer :birthdate
      t.integer :disappearance_date
      t.references :street, foreign_key: true
      t.string :mother
      t.string :dad
      t.boolean :status, default: true

      t.timestamps
    end
  end
end
