class CreateHistorics < ActiveRecord::Migration[5.2]
  def change
    create_table :historics do |t|
      t.string :description
      t.text :text
      t.references :disappeared, foreign_key: true

      t.timestamps
    end
  end
end
