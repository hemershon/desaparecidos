json.extract! disappeared, :id, :user_id, :name, :gender, :birthdate, :disappearance_date, :street_id, :mother, :dad, :created_at, :updated_at
json.url disappeared_url(disappeared, format: :json)
