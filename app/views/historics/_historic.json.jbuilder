json.extract! historic, :id, :description, :text, :disappeared_id, :created_at, :updated_at
json.url historic_url(historic, format: :json)
