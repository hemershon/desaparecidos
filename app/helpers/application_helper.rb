module ApplicationHelper
  def submit_button(type_button, icon, text)
    button_tag(type: "submit", class: "btn btn-#{type_button}") do
       "<span>#{text}</span>
        <i class='#{icon}'></i>".html_safe
     end
  end
end
