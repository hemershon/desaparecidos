$(document).on("turbolinks:load", function () {
    $(".show_modals").click(function () {
        findDisappeared(this.value);
    })
});

function dynamicSearch() {
    var input, filter, cards_mother, card, li, i;
    input = document.getElementById("input_search");
    filter = input.value.toUpperCase();
    cards_mother = document.getElementById("cards_mother");
    card = cards_mother.getElementsByClassName("my_card");
    for (i = 0; i < card.length; i++) {
        li = card[i].getElementsByClassName("card-title")[0];
        if (li.innerHTML.toUpperCase().indexOf(filter) > -1) {
            card[i].style.display = "";
        } else {
            card[i].style.display = "none";
        }
    }
}

function findDisappeared(disappeared_id) {
    $.ajax({
        url: "/disappeareds/" + disappeared_id,
        type: 'GET',
        dataType: 'json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        },
        success: function (json) {
            buildModal(json.disappeared);
        }
    });
}

function buildModal(disappeared_data) {
    if (disappeared_data.gender == true) {
        var gender = "Masculino";
    } else {
        var gender = "Feminino";
    }
    var template_string = `<div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title text-center"><strong>${disappeared_data.name}</strong></h1>
        <a href="/" class="close">
          <span aria-hidden="true">&times;</span>
        </a>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
                <label for="disappeared_dad">Pai</label>
                <input type="text" class="form-control" id="disappeared_dad" value="${disappeared_data.dad}" disabled>
            </div>
            <div class="col-md-6">
                <label for="disappeared_mom">Mãe</label>
                <input type="text" class="form-control" id="disappeared_mom" value="${disappeared_data.mother}" disabled>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <label for="disappeared_gender">Genero</label>
                <input type="text" class="form-control" id="disappeared_gender" value="${gender}" disabled>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <label for="disappeared_street">Localidade</label>
                <input type="text" class="form-control" id="disappeared_street" value="${getStreet(disappeared_data.street_id)}" disabled>
            </div>
         </div>
        <div class="row">
            <div class="col-md-6">
                <label for="disappeared_gender">Data de Desaparecimento</label>
                <input type="text" class="form-control date" id="disappeared_gender" value="${disappeared_data.disappearance_date}" disabled>
            </div>
            <div class="col-md-6">
                <label for="disappeared_mom">Data de Nascimento</label>
                <input type="text" class="form-control date" id="disappeared_mom" value="${disappeared_data.birthdate}" disabled>
            </div>
        </div>
       </div>
      <div class="modal-footer">
        <a class="btn btn-danger" href="/"><i class="fas fa-times-circle"></i> Fechar</a>
      </div>
    </div>
  </div>`;
    $("#show_disappeared_modal").html(template_string);
    startDate();
}

function getStreet(street_id) {
    var street = "";
    $.ajax({
        url: "/streets/" + street_id,
        type: 'GET',
        async: false,
        dataType: 'json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
        },
        success: function (json) {
            street = json.street;
        }
    });
    return street;
}
