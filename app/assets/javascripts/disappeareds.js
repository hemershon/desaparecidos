// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).on("turbolinks:load", function () {});

function FindCEP(cep){
    var cep = cep.replace("-", "");
    if (cep.length == 8) {
        $.getJSON("https://viacep.com.br/ws/" + cep + "/json/", function (dados) {
            if (!("erro" in dados)) {
                var template_string = `<div class='col-md-4'>` +
                                            `<label for='disappeared_street'>Rua</label>` +
                                            `<input type='text' class='form-control' name="disappeared[street]" id="disappeared_street" value="${dados.logradouro}">` +
                                      `</div>` +
                                      `<div class='col-md-3'>` +
                                            `<label for='disappeared_district'>Bairro</label>` +
                                            `<input type='text' class='form-control' name="disappeared[district]" id="disappeared_district" value="${dados.bairro}">` +
                                      `</div>` +
                                      `<div class='col-md-3'>` +
                                            `<label for='disappeared_city'>Cidade</label>` +
                                            `<input type='text' class='form-control' name="disappeared[city]" id="disappeared_city" value="${dados.localidade}">` +
                                      `</div>` +
                                      `<div class='col-md-2'>` +
                                            `<label for='disappeared_uf'>UF</label>` +
                                            `<input type='text' class='form-control' name="disappeared[uf]" id="disappeared_uf" value="${dados.uf}">` +
                                      `</div>`;

            } else {
                var template_string = "<div class='col-md-12'>" +
                                        "<span class='error' style='position: absolute; top: -5px;'>Por favor insira um CEP válido</span>" +
                                      "</div>";
            }
            $("#result_cep").html(template_string);
        });
    }
}