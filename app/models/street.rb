class Street < ApplicationRecord
  belongs_to :district
  has_many :disappeareds

  def to_s
    "#{district.city} - #{district.city.state}"
  end
end
