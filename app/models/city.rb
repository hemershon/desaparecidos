class City < ApplicationRecord
  belongs_to :state
  has_many :districts

  def to_s
    name
  end
end
