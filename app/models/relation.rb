class Relation < ApplicationRecord
  belongs_to :user
  belongs_to :disappeared
  belongs_to :parent
  has_many :relatives
end
