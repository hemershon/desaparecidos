class State < ApplicationRecord
  belongs_to :country
  has_many :cities

  def to_s
    name
  end
end
