class Disappeared < ApplicationRecord
 
  
  has_one_attached :avatar
  belongs_to :user
  belongs_to :street
  has_many :historics
  has_many :relations

  validates :name, presence: true
  validates :disappearance_date, presence: true
  validates :avatar, presence: true
end
