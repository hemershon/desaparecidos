class District < ApplicationRecord
  belongs_to :city
  has_many :streets

  def to_s
    name
  end
end
