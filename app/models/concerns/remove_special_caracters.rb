module RemoveSpecialCharacters
  extend ActiveSupport::Concern

  def birthdate=(value)
    write_attribute(:birthdate, value.gsub(/[^\w]/, "")) unless value.blank? || value.valid?
  end

  def disappearance_date=(value)
    write_attribute(:disappearance_date, value.gsub(/[^\w]/, "")) unless value.blank? || value.valid?
  end
end
