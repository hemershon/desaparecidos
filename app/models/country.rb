class Country < ApplicationRecord
  has_many :states
  def to_s
    name
  end
end
