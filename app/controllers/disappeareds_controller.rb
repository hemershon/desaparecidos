class DisappearedsController < ApplicationController
  before_action :set_disappeared, only: [:show, :edit, :update, :destroy]
  include AuxiliarBusinessRules

  def index
    @disappeareds = Disappeared.all
  end

  def show
    render json: {disappeared: @disappeared}
  end

  def new
    @disappeared = Disappeared.new
  end

  def edit; end

  def create
    create_street_attributes(disappeared_params) unless find_street_attributes(disappeared_params)
    street = find_street_attributes(disappeared_params) ? Street.find_by(name: disappeared_params[:street]).id : create_street_attributes(disappeared_params).id
    @disappeared = Disappeared.new(disappeared_params.except(:street, :city, :uf, :district).merge(user_id: current_user.id, street_id: street, gender: false)) if params[:gender]
    @disappeared = Disappeared.new(disappeared_params.except(:street, :city, :uf, :district).merge(user_id: current_user.id, street_id: street)) unless params[:gender]

    respond_to do |format|
      if @disappeared.save
        format.html {redirect_to root_path, notice: "Desaparecido adicionado com sucesso"}
        format.json {render :show, status: :created, location: @disappeared}
      else
        format.html {render :new}
        format.json {render json: @disappeared.errors, status: :unprocessable_entity}
      end
    end
  end

  def update
    respond_to do |format|
      if @disappeared.update(disappeared_params)
        format.html {redirect_to @disappeared, notice: 'Disappeared was successfully updated.'}
        format.json {render :show, status: :ok, location: @disappeared}
      else
        format.html {render :edit}
        format.json {render json: @disappeared.errors, status: :unprocessable_entity}
      end
    end
  end

  def destroy
    @disappeared.destroy
    respond_to do |format|
      format.html {redirect_to disappeareds_url, notice: 'Disappeared was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private

  def set_disappeared
    @disappeared = Disappeared.find(params[:id])
  end

  def disappeared_params
    params.require(:disappeared).permit(:user_id, :name, :gender, :birthdate, :disappearance_date, :mother, :dad, :cep, :district, :street, :city, :uf, :avatar)
  end
end
