module AuxiliarBusinessRules
  def find_street_attributes(all_params)
    find_street_attributes = true
    find_street_attributes = false if Street.find_by(name: all_params[:street]).nil? || District.find_by(name: all_params[:district]).nil?
    find_street_attributes
  end

  def create_street_attributes(all_params)
    city = City.find_by(name: all_params[:city])
    district = District.create(name: all_params[:district], city_id: city.id)
    street = Street.create(name: all_params[:street], district_id: district.id, cep: all_params[:cep])
    street
  end

end