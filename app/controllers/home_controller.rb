class HomeController < ApplicationController
  skip_before_action :authenticate_user!

  def index
    @disappeareds = Disappeared.all
  end
end
