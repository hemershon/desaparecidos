class StreetsController < ApplicationController
  def show
    street = Street.find(params[:id])
    render json: {street: "#{street.name} - #{street.district.name} - #{street.district.city.name} - #{street.district.city.state.name}"}
  end
end